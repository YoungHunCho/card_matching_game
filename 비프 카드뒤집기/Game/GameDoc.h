
// GameDoc.h : CGameDoc 클래스의 인터페이스
//


#pragma once


class CGameDoc : public CDocument
{

protected: // serialization에서만 만들어집니다.
	CGameDoc();
	DECLARE_DYNCREATE(CGameDoc)

// 특성입니다.
public:

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// 구현입니다.
public:
	virtual ~CGameDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()

#ifdef SHARED_HANDLERS
	// 검색 처리기에 대한 검색 콘텐츠를 설정하는 도우미 함수
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
public:
	CBitmap m_bmp[6][6]; //비트맵을 저장한다.
	CSize m_bmCell; //셀에 들어갈 그림 하나의 크기
	int m_nRow;	//격자의 초기 행 열 값
	int m_nCol;
	//그림위치 랜덤 값 저장
	int m_nRnd[36];
	//그림의 id 값 저장
	UINT m_nBmpID[6][6];
	bool m_bRandom;//게임중 / 랜덤의 유무 파악
	void ResizeWindow();
	void OnRandom();
	bool m_bShow[6][6]; //그림 보일까 말까
	int m_nRowIndex;
	int m_nColIndex;
	UINT m_nBmpFirstID; //처음 눌린 그림 ID
	UINT m_nBmpSecondID;//값이 매치 하는지 알아보려고
	bool m_bMouse; //true 면 첫 클릭 flase 면 두번째 클릭
	///////////////////
	/*unsigned int m_nGameTimer;
	unsigned int m_nGameStartTimer;
	int m_nGameStage;*/
	
	/////////////////키보드입력
	CBitmap keyboard;
	int keyboard_x, keyboard_y;
	bool onoff;
	/////////////////

	int theme;
	bool reset;



	////////////1차 합성
	unsigned int m_nGameTimer;
	unsigned int m_nTempTimer;
	unsigned int m_nGameStartTimer;
	unsigned int m_nGameStageTimer[4];
	int m_nGameStage;
	unsigned int m_nGamePoint;
	/////////////

	//////////2차 성징
	int m_nPlusMinusPoint;

	///////////

	//////////제발 마지막 합성이길
	bool m_bonus;
	int m_bonus_game;
	int m_bonus_time;
	/////////////////////

	CString name;
};

