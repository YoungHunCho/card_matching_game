
// GameView.cpp : CGameView 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "Game.h"
#endif

#include "GameDoc.h"
#include "GameView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include "THEMEDlg.h"

// CGameView

IMPLEMENT_DYNCREATE(CGameView, CView)

BEGIN_MESSAGE_MAP(CGameView, CView)
	// 표준 인쇄 명령입니다.
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_LBUTTONDOWN()
	ON_WM_CREATE()
	ON_WM_TIMER()
	ON_WM_KEYDOWN()
	ON_COMMAND(ID_VIEW_KEYBOARD, &CGameView::OnViewKeyboard)
	ON_COMMAND(ID_STOP, &CGameView::OnStop)
	ON_COMMAND(ID_START, &CGameView::OnStart)
END_MESSAGE_MAP()

// CGameView 생성/소멸

CGameView::CGameView()
	: m_nIsStop(false)
{
	// TODO: 여기에 생성 코드를 추가합니다.
	nMatchCount = 0;

}

CGameView::~CGameView()
{
}

BOOL CGameView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CView::PreCreateWindow(cs);
}

// CGameView 그리기

void CGameView::OnDraw(CDC* pDC)
{
	CGameDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 여기에 원시 데이터에 대한 그리기 코드를 추가합니다.

	CRect window;
	GetClientRect(window);
	CString str;
	str.Format(_T("%d.%d 초"), pDoc->m_nGameTimer / 10, pDoc->m_nGameTimer % 10);
	pDC->TextOutW((window.Width() / 2), 20, str);
	////////////////// 점수 플러스 마이너스

	CString strPoint;


	if (pDoc->m_nPlusMinusPoint != 0){
		if (!pDoc->m_nTempTimer){
			pDoc->m_nTempTimer = pDoc->m_nGameTimer;

		}
		if ((pDoc->m_nTempTimer - pDoc->m_nGameTimer) > 5){
			pDoc->m_nTempTimer = 0;
			pDoc->m_nPlusMinusPoint = 0;
			strPoint.Format(_T("       "));
		}
		if (pDoc->m_nPlusMinusPoint>0)
			strPoint.Format(_T("+%d"), pDoc->m_nPlusMinusPoint / 10);
		else if (pDoc->m_nPlusMinusPoint<0)
			strPoint.Format(_T("%d"), pDoc->m_nPlusMinusPoint / 10);

	}
	pDC->TextOutW((window.Width() / 2) + 60, 20, strPoint);

	///////////////// 점수 플러스 마이너스

	///////////////프로그래스바
	CClientDC dc(this);
	CDC mDC;
	mDC.CreateCompatibleDC(&dc);
	CBitmap bitmap;
	if (pDoc->m_nGameTimer > 100 || (pDoc->m_nGameTimer / 2) % 2)
		bitmap.LoadBitmapW(IDB_BITMAP_BLACK);
	else
		bitmap.LoadBitmapW(IDB_BITMAP_RED);

	mDC.SelectObject(&bitmap);
	float temp = pDoc->m_nGameTimer*((float)window.Width() / pDoc->m_nGameStageTimer[pDoc->m_nGameStage]);
	dc.StretchBlt(window.left, window.top, temp, 10, &mDC, 0, 0, 50, 50, SRCCOPY);
	/////////////////프로그래스 바

	//랜덤하게 되어있지 않다면 랜덤함수 호출
	if (pDoc->m_bRandom){
		pDoc->OnRandom();
		pDoc->m_bRandom = false;
	}
	int nCount = 0;

	CDC memDC;
	CBitmap* pOldBmp = NULL;
	memDC.CreateCompatibleDC(pDC);

	int a;
	if (pDoc->theme == 0)			a = IDB_NUMBER_BACK;
	else if (pDoc->theme == 1)		a = IDB_POKET_BACK;
	else							a = IDB_NUMBER_BACK;

	if (!pDoc->m_bonus){
		for (int n = 0; n < pDoc->m_nRow; n++){
			for (int m = 0; m < pDoc->m_nCol; m++){
				if (pDoc->m_bShow[n][m] == true)
				{
					pDoc->m_bmp[n][m].LoadBitmap((pDoc->m_nRnd[nCount] == IDB_BOOM) ? IDB_BOOM : pDoc->m_nRnd[nCount] + a);
					pOldBmp = memDC.SelectObject(&pDoc->m_bmp[n][m]);

					//왼쪽과 위쪽에 여백을 위해 1 더함
					pDC->BitBlt(pDoc->m_bmCell.cx*(m + 1), pDoc->m_bmCell.cy*(n + 1),//출력될 위치
						pDoc->m_bmCell.cx, pDoc->m_bmCell.cy,//출력될 그림 하나의 위치
						&memDC, 0, 0, SRCCOPY);
					pDoc->m_bmp[n][m].DeleteObject();
				}
				nCount++; //서로 다른 그림이 나타나도록 그림의 아이디 값을 1씩 증가
				if (pDoc->m_bShow[n][m] == false){
					CBitmap bmp;
					bmp.LoadBitmap(a); //노란 카트 출력
					pOldBmp = memDC.SelectObject(&bmp);
					pDC->BitBlt(pDoc->m_bmCell.cx*(m + 1), pDoc->m_bmCell.cy*(n + 1),//출력될 위치
						pDoc->m_bmCell.cx, pDoc->m_bmCell.cy,//출력될 그림 하나의 위치
						&memDC, 0, 0, SRCCOPY);
					bmp.DeleteObject();
				}
			}
		}
		memDC.SelectObject(pOldBmp);
		if (pDoc->reset){
			Sleep(500);
			pDoc->m_bRandom = true;
			for (int n = 0; n < pDoc->m_nRow; n++){
				for (int m = 0; m < pDoc->m_nCol; m++){
					pDoc->m_bShow[n][m] = false;
				}
			}
			nMatchCount = 0;
			pDoc->reset = false;
		}
		

		//////////////////////
		if (pDoc->onoff){
			pDoc->keyboard.LoadBitmap(IDB_KEYBOARD);

			pOldBmp = memDC.SelectObject(&pDoc->keyboard);
			pDC->TransparentBlt(pDoc->keyboard_x, pDoc->keyboard_y, 79, 81, &memDC, 0, 0, 79, 81, RGB(255, 255, 255));
			pDoc->keyboard.DeleteObject();
		}
		//////////////////////

		pDoc->ResizeWindow(); // doc 에 정의한 함수
	}
}


// CGameView 인쇄

BOOL CGameView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 기본적인 준비
	return DoPreparePrinting(pInfo);
}

void CGameView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄하기 전에 추가 초기화 작업을 추가합니다.
}

void CGameView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄 후 정리 작업을 추가합니다.
}


// CGameView 진단

#ifdef _DEBUG
void CGameView::AssertValid() const
{
	CView::AssertValid();
}

void CGameView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CGameDoc* CGameView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGameDoc)));
	return (CGameDoc*)m_pDocument;
}
#endif //_DEBUG


// CGameView 메시지 처리기


void CGameView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CGameDoc* pDoc = GetDocument();

	//////////////멈춘 상태 인지 확인
	if (m_nIsStop)
		return;
	//////////////멈춘 상태 인지 확인

	/////////////////타이머 시작
	if (pDoc->m_nGameTimer == pDoc->m_nGameStartTimer)
		pDoc->m_nGameTimer--;
	////////////////
	pDoc->m_nRowIndex = point.y / pDoc->m_bmCell.cy - 1;
	pDoc->m_nColIndex = point.x / pDoc->m_bmCell.cx - 1;

	if (pDoc->m_bShow[pDoc->m_nRowIndex][pDoc->m_nColIndex] == true
		|| pDoc->m_nBmpSecondID != 0
		|| pDoc->m_nRowIndex > pDoc->m_nRow - 1
		|| pDoc->m_nColIndex > pDoc->m_nCol - 1
		|| pDoc->m_nRowIndex < 0
		|| pDoc->m_nColIndex < 0)
		return;

	//인덱스 값의 그림을 보여줌
	pDoc->m_bShow[pDoc->m_nRowIndex][pDoc->m_nColIndex] = true;

	CRect rect;
	SetRect(&rect, pDoc->m_nRowIndex*(pDoc->m_bmCell.cx + 1),
		pDoc->m_nColIndex*(pDoc->m_bmCell.cy + 1),
		pDoc->m_nRowIndex*(pDoc->m_bmCell.cx + 2),
		pDoc->m_nColIndex*(pDoc->m_bmCell.cy + 2));
	InvalidateRect(rect);

	//마우스가 클릭한 곳의 인댁스 및 그림 아이디 저장
	if (pDoc->m_nBmpFirstID == 0){
		//첫 클릭
		pDoc->m_nBmpFirstID =
			pDoc->m_nBmpID[pDoc->m_nRowIndex][pDoc->m_nColIndex];
		m_nRowTempIndex = pDoc->m_nRowIndex;
		m_nColTempIndex = pDoc->m_nColIndex;
		pDoc->m_bMouse = true;
	}
	else
	{
		//second click
		pDoc->m_nBmpSecondID =
			pDoc->m_nBmpID[pDoc->m_nRowIndex][pDoc->m_nColIndex];
		pDoc->m_bMouse = false;
	}

	//같은 그림인지 판단 한다.
	OnMatching();

	CView::OnLButtonDown(nFlags, point);
}


void CGameView::OnMatching()
{
	CGameDoc* pDoc = GetDocument();

	int a;
	if (pDoc->theme == 0)			a = IDB_NUMBER_BACK;
	else if (pDoc->theme == 1)		a = IDB_POKET_BACK;
	else							a = IDB_NUMBER_BACK;


	//if those picture are same
	if (pDoc->m_nBmpFirstID == pDoc->m_nBmpSecondID
		&& pDoc->m_bMouse == false){
		int a = pDoc->m_nBmpFirstID;
		//reset the click event
		pDoc->m_bShow[m_nRowTempIndex][m_nColTempIndex] = true;
		pDoc->m_bShow[pDoc->m_nRowIndex][pDoc->m_nColIndex] = true;
		pDoc->m_nBmpFirstID = pDoc->m_nBmpSecondID = 0;
		m_nRowTempIndex = m_nColTempIndex = 0;

		if (a == IDB_BOOM){
			pDoc->reset = true;
			nMatchCount = 0;
		}
		else{
			////////////+- 점수
			pDoc->m_nPlusMinusPoint = 50;
			if (pDoc->m_nGameTimer + pDoc->m_nPlusMinusPoint < pDoc->m_nGameStartTimer)
				pDoc->m_nGameTimer += pDoc->m_nPlusMinusPoint;
			else
				pDoc->m_nGameTimer = pDoc->m_nGameStartTimer - 1;
			////////////+- 점수
		}
		nMatchCount++;
	}
	else if (pDoc->m_nBmpFirstID != pDoc->m_nBmpSecondID
		&& pDoc->m_bMouse == false){
		//show picture if those are diffrent picture
		CDC* pDC = GetDC();
		CDC memDC;
		CBitmap bmp;

		memDC.CreateCompatibleDC(pDC);
		bmp.LoadBitmap((pDoc->m_nBmpSecondID == IDB_BOOM) ? IDB_BOOM : pDoc->m_nBmpSecondID + a);

		CBitmap* pOldBmp = memDC.SelectObject(&bmp);
		pDC->BitBlt(pDoc->m_bmCell.cx * (pDoc->m_nColIndex + 1),
			pDoc->m_bmCell.cy*(pDoc->m_nRowIndex + 1),
			pDoc->m_bmCell.cx, pDoc->m_bmCell.cy,
			&memDC, 0, 0, SRCCOPY);
		bmp.DeleteObject();
		memDC.SelectObject(pOldBmp);
		Sleep(400);

		//reset click event
		pDoc->m_bShow[m_nRowTempIndex][m_nColTempIndex] = false;
		pDoc->m_bShow[pDoc->m_nRowIndex][pDoc->m_nColIndex] = false;
		pDoc->m_nBmpFirstID = pDoc->m_nBmpSecondID = 0;
		m_nRowTempIndex = m_nColTempIndex = 0;
		
		///////////////+- 점수
		pDoc->m_nPlusMinusPoint = -20;
		if (pDoc->m_nGameTimer > 20)// + pDoc->m_nPlusMinusPoint > 0)
			pDoc->m_nGameTimer += pDoc->m_nPlusMinusPoint;
		else
			pDoc->m_nGameTimer = 1;
		//////////////////+- 점수
	}
	Invalidate();
	if (nMatchCount + 1 == (pDoc->m_nRow * pDoc->m_nCol) / 2){
		OnSuccess();
	}
}


void CGameView::OnSuccess()
{
	CGameDoc* pDoc = GetDocument();
	CString str;
	KillTimer(1);
	pDoc->m_nGamePoint += pDoc->m_nGameTimer;
	str.Format(_T(" 스테이지 %d 클리어! \n누적 점수 : %d \n테마를 바꾸시겠습니까?")
		, pDoc->m_nGameStage + 1
		, pDoc->m_nGamePoint
		);
	int res = AfxMessageBox(str, MB_YESNO);
	if (res == IDYES){
		CTHEMEDlg theme;
		if (theme.DoModal() == IDOK){
			if (theme.m_theme == 0)		pDoc->theme = 0;
			else if (theme.m_theme == 1)	pDoc->theme = 1;
		}
	}
	//////////////////
	if (pDoc->m_nRow < 6 || pDoc->m_nCol < 6){
		pDoc->m_nGameStage++;
		if (pDoc->m_nRow < 6)
			pDoc->m_nRow++;
		if (pDoc->m_nCol < 6)
			pDoc->m_nCol++;
		pDoc->m_nGameTimer = pDoc->m_nGameStageTimer[pDoc->m_nGameStage];
		pDoc->m_nGameStartTimer = pDoc->m_nGameTimer;
		/////////////////
		if (pDoc->m_nGameStage == 1){
			pDoc->m_bonus = true;
			AfxMessageBox(_T("점수 보너스"), MB_OK | MB_ICONINFORMATION);
		}
		////////////////
	}
	else{//완전히 클리어
		str.Format(_T("축하드립니다 스테이지를 모두 클리어 하셨습니다!!!!\n\t 점수 %d")
			, pDoc->m_nGamePoint
			);
		AfxMessageBox(str, MB_OK);
		exit(0);
	}
	////////////////////
	pDoc->m_bRandom = true; //랜덤하게 그림 ID 변경

	for (int n = 0; n < pDoc->m_nRow; n++){
		for (int m = 0; m < pDoc->m_nCol; m++){
			pDoc->m_bShow[n][m] = false;
		}
	}
	//0 일때는 아무 값도 선택X
	pDoc->m_nBmpFirstID = pDoc->m_nBmpSecondID = 0;
	pDoc->m_bMouse = false;
	nMatchCount = 0;
	/////////////
	pDoc->m_nGameTimer = pDoc->m_nGameStartTimer;
	SetTimer(1, 100, NULL);
	////////////

	Invalidate();

}
#include "MainFrm.h"
#include "StartDlg.h"
int CGameView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	CGameDoc* pDoc = GetDocument();

	CStartDlg a;
	if (a.DoModal() == IDOK){
		if (a.m_Start_theme == 0)	pDoc->theme = 0;
		else						pDoc->theme = 1;

		if (a.m_Start_level == 0)
			for (int i = 0; i < 4; i++)	pDoc->m_nGameStageTimer[i] -= 100;
		else if (a.m_Start_level == 2)
			for (int i = 0; i < 4; i++)	pDoc->m_nGameStageTimer[i] += 100;

		pDoc->m_nGameTimer = pDoc->m_nGameStageTimer[0];
		pDoc->m_nGameStartTimer = pDoc->m_nGameStageTimer[0];

		if (a.m_Start_keyboard)			pDoc->onoff = true;
		else						pDoc->onoff = false;

		pDoc->name = a.m_Start_name;
		
		SetTimer(1, 100, NULL);
	}
	else{
		AfxMessageBox(_T("게임이 종료됩니다.\n"),MB_OK | MB_ICONSTOP);
		exit(0);
	}
	return 0;
}


void CGameView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CGameDoc* pDoc = GetDocument();
	CMainFrame *pFrame = (CMainFrame*)AfxGetMainWnd();
	CString temp;
	if (pDoc->m_nGameStageTimer[0] == 300)	pFrame->GetStatusBar()->SetPaneText(0, _T("난이도 : 중급"));
	else if (pDoc->m_nGameStageTimer[0] == 400)	pFrame->GetStatusBar()->SetPaneText(0, _T("난이도 : 하급"));
	else if (pDoc->m_nGameStageTimer[0] == 200)	pFrame->GetStatusBar()->SetPaneText(0, _T("난이도 : 상급"));
	temp.Format(_T("이름 : %s"), pDoc->name);
	pFrame->GetStatusBar()->SetPaneText(1,temp );
	temp.Format(_T("점수 : %d"), pDoc->m_nGamePoint);
	pFrame->GetStatusBar()->SetPaneText(2, temp);
	pFrame->GetStatusBar()->SetPaneText(3, _T("진행"));

	if (!pDoc->m_bonus){
		if (pDoc->m_nGameTimer != pDoc->m_nGameStartTimer)
			pDoc->m_nGameTimer--;
		if (pDoc->m_nGameTimer <= 0)
			OnFail();
		Invalidate();
	}
	else{
		if (!pDoc->m_bonus_time)	{
			pDoc->m_bonus = false;
			if (pDoc->m_bonus_game == 2)	pDoc->m_nGamePoint = pDoc->m_nGamePoint * 2;
			else if (pDoc->m_bonus_game == 0)	pDoc->m_nGamePoint *= 0.5;
			Sleep(2000);
			///////////////////////////////////////////////////////////////////////  .5 1 2 1
		}
		pDoc->m_bonus_time--;
		pDoc->m_bonus_game++;
		pDoc->m_bonus_game %= 4;
		OnBonusGame();
	}
	CView::OnTimer(nIDEvent);
}


void CGameView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CGameDoc* pDoc = GetDocument();

	if (pDoc->onoff){
		if (m_nIsStop)
			return;
		/////////////////타이머 시작
		if (pDoc->m_nGameTimer == pDoc->m_nGameStartTimer)
			pDoc->m_nGameTimer--;
		////////////////

		CRect rect;
		switch (nChar)
		{
		case VK_RIGHT:
			if (pDoc->keyboard_x < pDoc->m_bmCell.cx * pDoc->m_nCol)
				pDoc->keyboard_x += pDoc->m_bmCell.cx;
			else
				pDoc->keyboard_x = pDoc->m_bmCell.cx;				Invalidate();	break;
		case VK_LEFT:
			if (pDoc->keyboard_x > pDoc->m_bmCell.cx)
				pDoc->keyboard_x -= pDoc->m_bmCell.cx;	
			else
				pDoc->keyboard_x = pDoc->m_bmCell.cx * pDoc->m_nCol; Invalidate();	break;
		case VK_DOWN:
			if (pDoc->keyboard_y < pDoc->m_bmCell.cy * pDoc->m_nRow)
				pDoc->keyboard_y += pDoc->m_bmCell.cy;	
			else
				pDoc->keyboard_y = pDoc->m_bmCell.cy;				 Invalidate();	break;
		case VK_UP:
			if (pDoc->keyboard_y > pDoc->m_bmCell.cy)
				pDoc->keyboard_y -= pDoc->m_bmCell.cy;	
			else
				pDoc->keyboard_y = pDoc->m_bmCell.cy * pDoc->m_nRow; Invalidate();	break;
		//case VK_RETURN: case VK_SPACE:
		default:
			pDoc->m_nRowIndex = pDoc->keyboard_y / pDoc->m_bmCell.cy - 1;
			pDoc->m_nColIndex = pDoc->keyboard_x / pDoc->m_bmCell.cx - 1;

			if (pDoc->m_bShow[pDoc->m_nRowIndex][pDoc->m_nColIndex] == true
				|| pDoc->m_nBmpSecondID != 0
				|| pDoc->m_nRowIndex > pDoc->m_nRow - 1
				|| pDoc->m_nColIndex > pDoc->m_nCol - 1
				|| pDoc->m_nRowIndex < 0
				|| pDoc->m_nColIndex < 0)
				return;
			pDoc->m_bShow[pDoc->m_nRowIndex][pDoc->m_nColIndex] = true;


			SetRect(&rect, pDoc->m_nRowIndex * (pDoc->m_bmCell.cx + 1),
				pDoc->m_nColIndex * (pDoc->m_bmCell.cy + 1),
				pDoc->m_nRowIndex * (pDoc->m_bmCell.cx + 2),
				pDoc->m_nColIndex * (pDoc->m_bmCell.cy + 2));
			InvalidateRect(rect);

			if (pDoc->m_nBmpFirstID == 0){
				pDoc->m_nBmpFirstID = pDoc->m_nBmpID[pDoc->m_nRowIndex][pDoc->m_nColIndex];
				m_nRowTempIndex = pDoc->m_nRowIndex;
				m_nColTempIndex = pDoc->m_nColIndex;
				pDoc->m_bMouse = true;
			}
			else{
				pDoc->m_nBmpSecondID = pDoc->m_nBmpID[pDoc->m_nRowIndex][pDoc->m_nColIndex];
				pDoc->m_bMouse = false;
			}

			OnMatching();
			break;
		}
	}

	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}


void CGameView::OnViewKeyboard()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	CGameDoc* pDoc = GetDocument();

	if (pDoc->onoff)		pDoc->onoff = false;
	else					pDoc->onoff = true;
}


void CGameView::OnFail()
{
	CGameDoc* pDoc = GetDocument();
	CString str;
	KillTimer(1);
	str.Format(_T(" 스테이지 %d 실패! \nGAME OVER \n최종 점수 : %d \n다시 시도하시겠습니까?")
		, pDoc->m_nGameStage + 1
		, pDoc->m_nGamePoint
		);
	int res = AfxMessageBox(str, MB_YESNO);
	if (res == IDYES){

		pDoc->m_bRandom = true; //랜덤하게 그림 ID 변경

		for (int n = 0; n < pDoc->m_nRow; n++){
			for (int m = 0; m < pDoc->m_nCol; m++){
				pDoc->m_bShow[n][m] = false;
			}
		}
		//0 일때는 아무 값도 선택X
		pDoc->m_nBmpFirstID = pDoc->m_nBmpSecondID = 0;
		pDoc->m_bMouse = false;
		nMatchCount = 0;
		/////////////
		pDoc->m_nGameTimer = pDoc->m_nGameStartTimer;
		SetTimer(1, 100, NULL);
		//pDoc->m_nGameStage = 0;
		////////////

		Invalidate();
	}
	else{
		str.Format(_T("지금까지의 점수입니다.\n          점수 %d")
			, pDoc->m_nGamePoint
			);
		AfxMessageBox(str, MB_OK);
		exit(0);
	}
}


void CGameView::OnStop()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	CMainFrame *pFrame = (CMainFrame*)AfxGetMainWnd();
	pFrame->GetStatusBar()->SetPaneText(3, _T("정지"));

	m_nIsStop = true;
	KillTimer(1);
}




void CGameView::OnStart()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	CMainFrame *pFrame = (CMainFrame*)AfxGetMainWnd();
	pFrame->GetStatusBar()->SetPaneText(3, _T("진행"));

	m_nIsStop = false;
	SetTimer(1, 100, NULL);
}


void CGameView::OnBonusGame()
{
	CGameDoc* pDoc = GetDocument();
	CDC* pDC = GetDC();
	CRect rc;
	this->GetClientRect(&rc);
	int width = rc.Width();
	int height = rc.Height();
	rc.DeflateRect(width, height);

	pDC->Rectangle(&rc);
	CBrush* pOld = NULL;
	CBrush brush_0(RGB(255 - pDoc->m_bonus_time * 3, 0, 0)), brush_1(RGB(0, 255 - pDoc->m_bonus_time * 3, 0)), 
		   brush_2(RGB(0, 0, 255 - pDoc->m_bonus_time * 3)), brush_3(RGB(255 - pDoc->m_bonus_time * 3, 255 - pDoc->m_bonus_time * 3, 0));
	CFont font, *Old = NULL;
	switch (pDoc->m_bonus_game){
	case 0:
		pOld = pDC->SelectObject(&brush_0);		pDC->Rectangle(width / 4, height / 4, width / 2, height / 2);				pDC->SelectObject(pOld);
		pOld = pDC->SelectObject(&brush_1);		pDC->Rectangle(width / 2, height / 4, width / 4 * 3, height / 2);			pDC->SelectObject(pOld);
		pOld = pDC->SelectObject(&brush_2);		pDC->Rectangle(width / 4, height / 2, width / 2, height / 4 * 3);			pDC->SelectObject(pOld);
		font.CreateFont(40, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, _T("굴림체"));
		Old = pDC->SelectObject(&font);		pDC->TextOutW(width / 12 * 7, height / 12 * 7, _T("X0.5"));		pDC->SelectObject(pOld);	break;
	case 1:
		pOld = pDC->SelectObject(&brush_0);		pDC->Rectangle(width / 4, height / 4, width / 2, height / 2);				pDC->SelectObject(pOld);
		pOld = pDC->SelectObject(&brush_2);		pDC->Rectangle(width / 4, height / 2, width / 2, height / 4 * 3);			pDC->SelectObject(pOld);
		pOld = pDC->SelectObject(&brush_3);		pDC->Rectangle(width / 2, height / 2, width / 4 * 3, height / 4 * 3);		pDC->SelectObject(pOld);
		font.CreateFont(40, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, _T("굴림체"));
		Old = pDC->SelectObject(&font);		pDC->TextOutW(width / 12 * 7, height / 12 * 4, _T("X1.0"));		pDC->SelectObject(pOld);	break;
	case 2:
		pOld = pDC->SelectObject(&brush_1);		pDC->Rectangle(width / 2, height / 4, width / 4 * 3, height / 2);			pDC->SelectObject(pOld);
		pOld = pDC->SelectObject(&brush_2);		pDC->Rectangle(width / 4, height / 2, width / 2, height / 4 * 3);			pDC->SelectObject(pOld);
		pOld = pDC->SelectObject(&brush_3);		pDC->Rectangle(width / 2, height / 2, width / 4 * 3, height / 4 * 3);		pDC->SelectObject(pOld);
		font.CreateFont(40, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, _T("굴림체"));
		Old = pDC->SelectObject(&font);		pDC->TextOutW(width / 12 * 4, height / 12 * 4, _T("X2.0"));		pDC->SelectObject(pOld);	break;
	case 3:
		pOld = pDC->SelectObject(&brush_0);		pDC->Rectangle(width / 4, height / 4, width / 2, height / 2);				pDC->SelectObject(pOld);
		pOld = pDC->SelectObject(&brush_1);		pDC->Rectangle(width / 2, height / 4, width / 4 * 3, height / 2);			pDC->SelectObject(pOld);
		pOld = pDC->SelectObject(&brush_3);		pDC->Rectangle(width / 2, height / 2, width / 4 * 3, height / 4 * 3);		pDC->SelectObject(pOld);
		font.CreateFont(40, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, _T("굴림체"));
		Old = pDC->SelectObject(&font);		pDC->TextOutW(width / 12 * 4, height / 12 * 7, _T("X1.0"));		pDC->SelectObject(pOld);	 break;
	}
}
