//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// Game.rc에서 사용되고 있습니다.
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_GameTYPE                    130
#define IDB_BITMAP1                     310
#define IDB_NUMBER_BACK                 310
#define IDB_BITMAP2                     311
#define IDB_NUMBER_01                   311
#define IDB_BITMAP3                     312
#define IDB_NUMBER_02                   312
#define IDB_BITMAP4                     313
#define IDB_NUMBER_03                   313
#define IDB_BITMAP5                     314
#define IDB_NUMBER_04                   314
#define IDB_BITMAP6                     315
#define IDB_NUMBER_05                   315
#define IDB_BITMAP7                     316
#define IDB_NUMBER_06                   316
#define IDB_BITMAP8                     317
#define IDB_NUMBER_07                   317
#define IDB_BITMAP9                     318
#define IDB_NUMBER_08                   318
#define IDB_BITMAP10                    319
#define IDB_NUMBER_09                   319
#define IDB_BITMAP11                    320
#define IDB_NUMBER_10                   320
#define IDB_BITMAP12                    321
#define IDB_NUMBER_11                   321
#define IDB_BITMAP13                    322
#define IDB_NUMBER_12                   322
#define IDB_BITMAP14                    323
#define IDB_NUMBER_13                   323
#define IDB_BITMAP15                    324
#define IDB_NUMBER_14                   324
#define IDB_BITMAP16                    325
#define IDB_NUMBER_15                   325
#define IDB_BITMAP17                    326
#define IDB_NUMBER_16                   326
#define IDB_BITMAP18                    327
#define IDB_NUMBER_17                   327
#define IDB_BITMAP19                    328
#define IDB_NUMBER_18                   328
#define IDB_KEYBOARD                    329
#define IDB_POKET_BACK                  330
#define IDB_POKET_01                    331
#define IDB_POKET_02                    332
#define IDB_POKET_03                    333
#define IDB_POKET_04                    334
#define IDB_POKET_05                    335
#define IDB_POKET_06                    336
#define IDB_POKET_07                    337
#define IDB_POKET_08                    338
#define IDB_POKET_09                    339
#define IDB_POKET_10                    340
#define IDB_POKET_11                    341
#define IDB_POKET_12                    342
#define IDB_POKET_13                    343
#define IDB_POKET_14                    344
#define IDB_POKET_15                    345
#define IDB_POKET_16                    346
#define IDB_POKET_17                    347
#define IDB_POKET_18                    348
#define IDB_BOOM                        349
#define IDD_DIALOG1                     350
#define IDB_BITMAP_BLACK                351
#define IDB_BITMAP21                    352
#define IDB_BITMAP_RED                  352
#define IDD_DIALOG2                     353
#define IDC_EDIT1                       1002
#define IDC_RADIO1                      1003
#define IDC_RADIO2                      1004
#define IDC_RADIO3                      1005
#define IDC_RADIO4                      1006
#define IDC_RADIO5                      1007
#define IDC_RADIO6                      1008
#define IDC_RADIO7                      1009
#define ID_VIEW_KEYBOARD                32771
#define ID_32772                        32772
#define ID_VIEW_ONOFF                   32773
#define ID_VIEW_KEBOARD                 32774
#define ID_32775                        32775
#define ID_32776                        32776
#define ID_START                        32777
#define ID_STOP                         32778
#define ID_BUTTON32782                  32782

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        354
#define _APS_NEXT_COMMAND_VALUE         32783
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
