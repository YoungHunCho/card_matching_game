#pragma once


// CStartDlg 대화 상자입니다.

class CStartDlg : public CDialog
{
	DECLARE_DYNAMIC(CStartDlg)

public:
	CStartDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CStartDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CString m_Start_name;
	UINT m_Start_theme;
	UINT m_Start_level;
	UINT m_Start_keyboard;
};
