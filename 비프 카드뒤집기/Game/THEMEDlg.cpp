// THEMEDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Game.h"
#include "THEMEDlg.h"
#include "afxdialogex.h"


// CTHEMEDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTHEMEDlg, CDialog)

CTHEMEDlg::CTHEMEDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTHEMEDlg::IDD, pParent)
	, m_theme(0)
{

}

CTHEMEDlg::~CTHEMEDlg()
{
}

void CTHEMEDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_RADIO1, (int&) m_theme);
}


BEGIN_MESSAGE_MAP(CTHEMEDlg, CDialog)
END_MESSAGE_MAP()


// CTHEMEDlg 메시지 처리기입니다.

