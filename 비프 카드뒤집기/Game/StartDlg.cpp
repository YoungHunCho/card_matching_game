// StartDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Game.h"
#include "StartDlg.h"
#include "afxdialogex.h"


// CStartDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CStartDlg, CDialog)

CStartDlg::CStartDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CStartDlg::IDD, pParent)
	, m_Start_name(_T(""))
	, m_Start_theme(0)
	, m_Start_level(1)
	, m_Start_keyboard(1)
{

}

CStartDlg::~CStartDlg()
{
}

void CStartDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_Start_name);
	DDX_Radio(pDX, IDC_RADIO1, (int &) m_Start_theme);
	DDX_Radio(pDX, IDC_RADIO3, (int &) m_Start_level);
	DDX_Radio(pDX, IDC_RADIO6, (int &) m_Start_keyboard);
}


BEGIN_MESSAGE_MAP(CStartDlg, CDialog)
END_MESSAGE_MAP()


// CStartDlg 메시지 처리기입니다.
