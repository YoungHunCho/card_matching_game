
// GameDoc.cpp : CGameDoc 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "Game.h"
#endif

#include "GameDoc.h"

#include <propkey.h>

#include "GameView.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CGameDoc

IMPLEMENT_DYNCREATE(CGameDoc, CDocument)

BEGIN_MESSAGE_MAP(CGameDoc, CDocument)
END_MESSAGE_MAP()


// CGameDoc 생성/소멸

CGameDoc::CGameDoc()
{
	// TODO: 여기에 일회성 생성 코드를 추가합니다.
	srand((unsigned)time(NULL));
	m_bmCell = CSize(81, 83); //비트맵 크기값 설정
	m_nRow = 3;	//격자의 초기 행렬 값
	m_nCol = 4;
	m_bRandom = true; // 랜덤하게 그림 ID변경
	for (int n = 0; n < m_nRow; n++){
		for (int m = 0; m < m_nCol; m++){
			m_bShow[n][m] = false;
		}
	}
	m_nBmpFirstID = m_nBmpSecondID = 0;
	m_bMouse = false;
	m_nGameStage = 0;
	m_nTempTimer = 0;
	m_nGameStageTimer[0] = 300;
	m_nGameStageTimer[1] = 500;
	m_nGameStageTimer[2] = 1000;
	m_nGameStageTimer[3] = 1500;
	m_nGameTimer = m_nGameStageTimer[0];
	m_nGameStartTimer = m_nGameStageTimer[0];
	m_nGamePoint = 0;
	//////////////2차 성징
	m_nPlusMinusPoint = 0;
	/////////

	//////////////////////
	keyboard_x = m_bmCell.cx;
	keyboard_y = m_bmCell.cy;
	onoff = true;
	////////////////////

	theme = 0;
	reset = false;

	///////////////////////bonus
	m_bonus = false;
	m_bonus_game = 0;
	m_bonus_time = 20 + rand() % 4;
	///////////////////////
}

CGameDoc::~CGameDoc()
{
}

BOOL CGameDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: 여기에 재초기화 코드를 추가합니다.
	// SDI 문서는 이 문서를 다시 사용합니다.

	return TRUE;
}




// CGameDoc serialization
#include"GameView.h"
void CGameDoc::Serialize(CArchive& ar)
{
	CGameView* pView = (CGameView*)m_viewList.GetHead();
	if (ar.IsStoring())
	{
		// TODO: 여기에 저장 코드를 추가합니다.
		

		ar
			<< m_bmCell
			<< m_nRow
			<< m_nCol
			<< m_nRnd[36]
			<< m_nBmpID[6][6]
			<< m_bRandom
			<< m_bShow[6][6]
			<< m_nRowIndex
			<< m_nColIndex
			<< m_nBmpFirstID
			<< m_nBmpSecondID
			<< keyboard_x
			<< keyboard_y
			<< onoff
			<< theme
			<< reset
			<< m_nGameTimer
			<< m_nTempTimer
			<< m_nGameStartTimer
			<< m_nGameStageTimer[4]
			<< m_nGameStage
			<< m_nGamePoint
			<< m_nPlusMinusPoint
			<< pView->m_nRowTempIndex
			<< pView->m_nColTempIndex
			<< pView->nMatchCount
			<< pView->m_nIsStop
			<< m_bonus
			<< m_bonus_game
			<< m_bonus_time
			<< name;
	}
	else
	{
		// TODO: 여기에 로딩 코드를 추가합니다.
		ar
			>> m_bmCell
			>> m_nRow
			>> m_nCol
			>> m_nRnd[36]
			>> m_nBmpID[6][6]
			>> m_bRandom
			>> m_bShow[6][6]
			>> m_nRowIndex
			>> m_nColIndex
			>> m_nBmpFirstID
			>> m_nBmpSecondID
			>> keyboard_x
			>> keyboard_y
			>> onoff
			>> theme
			>> reset
			>> m_nGameTimer
			>> m_nTempTimer
			>> m_nGameStartTimer
			>> m_nGameStageTimer[4]
			>> m_nGameStage
			>> m_nGamePoint
			>> m_nPlusMinusPoint
			>> pView->m_nRowTempIndex
			>> pView->m_nColTempIndex
			>> pView->nMatchCount
			>> pView->m_nIsStop
			>> m_bonus
			>> m_bonus_game
			>> m_bonus_time
			>> name;
		UpdateAllViews(pView);
	}
}

#ifdef SHARED_HANDLERS

// 축소판 그림을 지원합니다.
void CGameDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// 문서의 데이터를 그리려면 이 코드를 수정하십시오.
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// 검색 처리기를 지원합니다.
void CGameDoc::InitializeSearchContent()
{
	CString strSearchContent;
	// 문서의 데이터에서 검색 콘텐츠를 설정합니다.
	// 콘텐츠 부분은 ";"로 구분되어야 합니다.

	// 예: strSearchContent = _T("point;rectangle;circle;ole object;");
	SetSearchContent(strSearchContent);
}

void CGameDoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = NULL;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != NULL)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CGameDoc 진단

#ifdef _DEBUG
void CGameDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CGameDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CGameDoc 명령


void CGameDoc::ResizeWindow()
{
	CMainFrame* pMain = (CMainFrame*)AfxGetMainWnd();

	CREATESTRUCT st;
	CREATESTRUCT& rst = st;

	pMain->PreCreateWindow(rst);

	rst.lpszName = _T("GAME");
	rst.cx = (m_nRow + 1)*m_bmCell.cx + 180;
	rst.cy = (m_nCol + 1)*m_bmCell.cy + 180;

	CRect rect;
	pMain->GetClientRect(rect);
	pMain->MoveWindow(rect.left, rect.top, rst.cx, rst.cy);
}


void CGameDoc::OnRandom()
{
	BOOL bInsert = TRUE;
	int nGrating = m_nRow*m_nCol; //총 그림 수

	for (int n = 0; n < nGrating; n++){
		m_nRnd[n] = (rand() % (nGrating / 2)) + 1;

		for (int m = 0; m < n; m++){
			if (m_nRnd[n] == m_nRnd[m]){
				if (bInsert == FALSE){
					n--;
					bInsert = TRUE;
					break;
				}
				bInsert = FALSE;
			}
		}
	}
	int temp;
	bool loop = true;
	do{
		temp = m_nRnd[rand() % nGrating + 1];
		int temp_count = 0;

		for (int i = 0; i < nGrating; i++)
			if (m_nRnd[i] != temp)	temp_count++;

		if (temp_count == nGrating - 2)
			break;
	} while (loop);

	
	for (int i = 0; i < nGrating; i++)
		if (m_nRnd[i] == temp) m_nRnd[i] = IDB_BOOM;
	
	int nCount = 0;

	for (int n = 0; n < m_nRow; n++){
		for (int m = 0; m < m_nCol; m++){
			m_nBmpID[n][m] = m_nRnd[nCount];
			nCount++;
		}
	}

}
